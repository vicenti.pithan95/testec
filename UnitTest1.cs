using ApiSusAV.Model;
using ApiSusAV.Controllers;
using System;
using Xunit;
using Bogus;

namespace TestUnit
{
    public class UnitTest
    {
        [Fact]
        [Trait("usuario","Criar")]
        public void PostUser()
        {
            var faker = new Faker("pt_BR");
            User user = new User();
            user.Username = faker.Person.FirstName;
            byte[] dadosAsBytes = System.Text.Encoding.ASCII.GetBytes(faker.Random.Word());
            user.Password = System.Convert.ToBase64String(dadosAsBytes);
            HomeController cliente = new HomeController();
            var teste = cliente.Create(user);
            User result = cliente.getUser(user.Username);
            Assert.Equal(user.Username, result.Username);
        }
        [Theory]
        [InlineData("Profissional da saude")]
        [Trait("Grupo Especial", "Criar")]
        public void PostGrupoEspecial(String value)
        {
            GrupoEspecial grupoDTO = new GrupoEspecial();
            grupoDTO.NmGrupo = value;
            grupoDTO.IsComorbidade = false;
            GrupoEspecialController grupo = new GrupoEspecialController();
            GrupoEspecial grupoDal = grupo.GetGrupoEspecialNmGrupo(value);
            if(grupoDal != null)
            {
                grupo.DeleteGrupoEspecial(grupoDal.Id);
            }
            grupo.CreateGrupoEspecial(grupoDTO);
            grupoDal = grupo.GetGrupoEspecialNmGrupo(value);
            Assert.Equal(value, grupoDal.NmGrupo);
        }
    }
}
